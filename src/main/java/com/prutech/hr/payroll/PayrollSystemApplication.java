package com.prutech.hr.payroll;

import java.util.List;
import java.util.concurrent.Executor;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.prutech.hr.util.HRContextListner;
import com.prutech.mailsender.model.MailServerDetails;
import com.prutech.mailsender.service.MailServerDetailsService;
import com.prutech.mailsender.util.MailSenderUtil;

@ComponentScan(basePackages = { "com.prutech" })
@EnableTransactionManagement
@EnableAsync
@SpringBootApplication(exclude = { LiquibaseAutoConfiguration.class, DataSourceAutoConfiguration.class,
		ValidationAutoConfiguration.class, HibernateJpaAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class,
		DataSourceTransactionManagerAutoConfiguration.class })

@Configuration
public class PayrollSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(PayrollSystemApplication.class, args);
	}

	@Autowired
	ApplicationContext applicationContext;

	@Autowired
	private MailServerDetailsService mailServerDetailsService;

	@Bean(name = "threadPoolExecutor")
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(2);
		executor.setMaxPoolSize(5);
		executor.setQueueCapacity(50);
		executor.setThreadNamePrefix("threadPoolExecutor-");
		executor.initialize();
		return executor;
	}

	/*
	 * @Autowired private AuthorityRepository authorityRepository;
	 * 
	 * @Autowired private RoleRepository roleRepository;
	 */

	private static final Logger LOGGER = Logger.getLogger(PayrollSystemApplication.class);

	@PostConstruct
	public void createMailSendersForAllOrganizations() {
		List<MailServerDetails> allActiveMailServerDetails = mailServerDetailsService.getAllActiveMailServerDetails();
		LOGGER.debug("PayrollSystemApplication.createMailSendersForAllOrganizations()...allActiveMailServerDetails:"
				+ allActiveMailServerDetails);
		for (MailServerDetails mailServerDetails : allActiveMailServerDetails) {

			LOGGER.debug(
					"PayrollSystemApplication.createMailSendersForAllOrganizations()...mailServerDetails.getCreatedDate():"
							+ mailServerDetails.getCreatedDate());
			LOGGER.debug(
					"PayrollSystemApplication.createMailSendersForAllOrganizations()...mailServerDetails.getLastModifiedDate():"
							+ mailServerDetails.getLastModifiedDate());

			// Storing fresh MailServerDetails
			MailSenderUtil.mailServerDetails.put(mailServerDetails.getOrganizationId(), mailServerDetails);

			// Storing JavaMailSenderImpl's
			MailSenderUtil.mailSenders.put(mailServerDetails.getOrganizationId(),
					MailSenderUtil.createMailSender(mailServerDetails));

			LOGGER.debug(
					"MailSenderApplication.createMailSendersForAllOrganizations()...mailServerDetails.getOrganizationId():"
							+ mailServerDetails.getOrganizationId());
			LOGGER.debug(
					"MailSenderApplication.createMailSendersForAllOrganizations()...mailServerDetails.getOrganizationId():"
							+ mailServerDetails.getOrganizationId());
			// Storing last modified dates
			MailSenderUtil.mailServerDetailsLastModifiedDates.put(mailServerDetails.getOrganizationId(),
					mailServerDetails.getLastModifiedDate());
			MailSenderUtil.mailSenders.forEach((key, value) -> LOGGER
					.debug("PayrollSystemApplication.createMailSendersForAllOrganizations()...key = " + key
							+ ", Value = " + value));
			// Need to store all organization action templates into collection
			MailSenderUtil.storeAllOrganizationTemplates(mailServerDetails.getOrganizationId(), applicationContext);

		}
	}

	@Bean
	public HRContextListner hrContextListner() {
		return new HRContextListner();
	}

	/*
	 * @Bean(value = "beanMapper") public DozerBeanMapper before() throws Exception
	 * { return new DozerBeanMapper(); }
	 */
	/*
	 * @Bean public PayrollContextListner payrollContextListner() { return new
	 * PayrollContextListner(); }
	 */

}

/*
 * @SpringBootApplication public class PayrollSystemApplication {
 * 
 * public static void main(String[] args) {
 * SpringApplication.run(PayrollSystemApplication.class, args); }
 * 
 * }
 */
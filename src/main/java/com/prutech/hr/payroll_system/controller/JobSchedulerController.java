package com.prutech.hr.payroll_system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prutech.hr.payroll_system.service.impl.SchedulerService;
import com.prutech.oauth2.annotation.SystemAuthority;
import com.prutech.scheduler.JobSchedulerService;
import com.prutech.scheduler.model.CronJob;

@RestController
@RequestMapping("/api/v1/jobScheduler")
public class JobSchedulerController {

	@Autowired
	JobSchedulerService mySchedulerService;

	@Autowired
	SchedulerService schedulerService;
	
	@SystemAuthority(name = "MANAGE SCHEDULER FOR SCHEDULED")
	@PreAuthorize("hasAuthority('MANAGE SCHEDULER FOR SCHEDULED')")
	@RequestMapping(value = "/schedule-cron-job", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public CronJob scheduleAJob(@RequestBody CronJob job) {
		Class<? extends QuartzJobBean> clazz;
		try {
			System.out.println("JobSchedulerController.scheduleAJob()" + job.getJobClazz());
			clazz = (Class<? extends QuartzJobBean>) Class.forName(job.getJobClazz());
			schedulerService.scheduleJob(job, clazz);
		} catch (Exception e) {
			return job.withResponse("unsuccessfull" + e.getMessage());
		}
		return job.withResponse("Successful");
	}

}

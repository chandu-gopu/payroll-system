package com.prutech.hr.payroll_system.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.DocumentException;
import com.prutech.hr.payroll.h2db.model.EmployeeSalary;
import com.prutech.hr.payroll.model.PaySlip;
import com.prutech.hr.payroll_system.oauth.config.AppModuleName;
import com.prutech.hr.payroll_system.service.impl.PayRollServices;

import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping("/api/v1/payrollserivice")
public class PayRollController implements AppModuleName {

	@Autowired
	private PayRollServices payRollService;

	/**
	 * 
	 * @return
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	@GetMapping("/generate-payroll")
	public ResponseEntity generatePayroll() throws InterruptedException, ExecutionException {
		return new ResponseEntity(payRollService.generatePayroll(), HttpStatus.OK);

	}

	@GetMapping("/get-payslips")
	public ResponseEntity getPaySlips() {
		List<EmployeeSalary> emplSalaries = payRollService.getEmployeeTempSalaries("DECEMBER-2019");
		return new ResponseEntity(emplSalaries, HttpStatus.OK);
	}

	@PostMapping("/approve-salaries")
	public ResponseEntity approveList(@RequestBody List<String> employeNumbersList) {
		List<PaySlip> paySlips = payRollService.ApproveSalariesAndSaveInDB(employeNumbersList);
		return new ResponseEntity(paySlips, HttpStatus.OK);
	}

	@GetMapping(value = "/generate-payslips")
	public ResponseEntity<String> generatePaySlips() throws JRException, IOException, SQLException {

		try {
			payRollService.generateEncryptedPayslips();
			ResponseEntity.ok().body("Success");
		} catch (DocumentException e) {

			ResponseEntity.status(500).body(e.getMessage());
		}

		return new ResponseEntity<>("Generating Payroll", HttpStatus.OK);
	}

}

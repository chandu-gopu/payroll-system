package com.prutech.hr.payroll_system.db_config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "h2entityManagerFactory", basePackages = {
		"com.prutech.hr.payroll.h2db.repository" }, transactionManagerRef = "h2transactionManager")
public class H2Config {

	@Bean(name = "h2datasorce")
	// @ConfigurationProperties(prefix = "spring.h2.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().url("jdbc:h2:mem:testdb").username("sa").driverClassName("org.h2.Driver")
				.password("").build();
	}

	@Bean(name = "h2entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
			@Qualifier("h2datasorce") DataSource dataSource) {

		EntityManagerFactoryBuilder builder = new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(),
				new HashMap<>(), null);

		Map<String, Object> properties = new HashMap<>();

		String[] packages = { "com.prutech.hr.payroll.h2db.model" };
		properties.put("hibernate.hbm2ddl.auto", "update");
		properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
		// properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.enable_lazy_load_no_trans", "true");
		return builder.dataSource(dataSource).properties(properties).packages(packages).build();

	}

	@Bean(name = "h2transactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("h2entityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

}

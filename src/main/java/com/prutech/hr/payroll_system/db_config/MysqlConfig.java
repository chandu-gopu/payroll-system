package com.prutech.hr.payroll_system.db_config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactory", basePackages = {
		"com.prutech.hr.payroll.repository", "com.prutech.hr.repository", "com.prutech.bulkprocessor.persistence",
		"com.prutech.mailsender.dao", "com.prutech.repository" }, transactionManagerRef = "transactionManager")
public class MysqlConfig {

	@Primary
	@Bean(name = "dataSource")
	// @ConfigurationProperties(prefix = "spring.mysql.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().url(
				"jdbc:mysql://localhost:3306/payroll_multipledb?allowPublicKeyRetrieval=true&useSSL=false&createDatabaseIfNotExist=true")
				.username("root").password("root").build();
	}

	@Primary
	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
			@Qualifier("dataSource") DataSource dataSource) {

		EntityManagerFactoryBuilder builder = new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(),
				new HashMap<>(), null);
		Map<String, Object> properties = new HashMap<>();
		String[] modelPakages = { "com.prutech.bulkprocessor.persistence", "com.prutech.hr.model",
				"com.prutech.mailsender.model", "com.prutech.hr.payroll.model", "com.prutech.model",
				"com.prutech.oauth2.model", "com.prutech.scheduler.model" };
		properties.put("hibernate.hbm2ddl.auto", "update");
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL8Dialect");
		// properties.put("hibernate.enable_lazy_load_no_trans", "true");
		return builder.dataSource(dataSource).properties(properties).packages(modelPakages).build();

	}

	@Primary
	@Bean(name = "transactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("entityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

}

package com.prutech.hr.payroll_system.job;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.prutech.constants.StatusEnum;
import com.prutech.hr.payroll.model.SalaryStructure;
import com.prutech.hr.payroll.repository.SalaryStructureRepository;

public class SalaryStatusUpdate extends QuartzJobBean {

	@Autowired
	private SalaryStructureRepository salaryStructureRepository;

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		Calendar c = Calendar.getInstance();
		Date currentTime = c.getTime();

		List<SalaryStructure> salaryStructureList = salaryStructureRepository.findAll();
		salaryStructureList.forEach(salaryStructure -> {
			Date scheduledDate = salaryStructure.getEffevtiveUpto();
			if (scheduledDate.compareTo(currentTime) < 0) {
				if ((salaryStructure.getStatus().getStatusCode() == StatusEnum.ACTIVE.getStatusCode())) {
					salaryStructure.setStatus(StatusEnum.INACTIVE);
					salaryStructureRepository.save(salaryStructure);
				}
			}

		});
	}
}

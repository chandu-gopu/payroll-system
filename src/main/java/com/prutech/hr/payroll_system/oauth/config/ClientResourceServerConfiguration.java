package com.prutech.hr.payroll_system.oauth.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.web.bind.annotation.CrossOrigin;

@Configuration
@CrossOrigin(origins = "*")
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ClientResourceServerConfiguration extends ResourceServerConfigurerAdapter {
	//
	private static final String RESOURCE_ID = "resource-server-rest-api";
	//
	private static final String SECURED_READ_SCOPE = "#oauth2.hasScope('read')";
	//
	private static final String SECURED_WRITE_SCOPE = "#oauth2.hasScope('write')";
	//
	private static final String SECURED_PATTERN = "/api/v1/**";

	/**
	 * 
	 */
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(RESOURCE_ID);
	}

	/**
	 * 
	 */
	@Override
	public void configure(HttpSecurity http) throws Exception {

		/**
		 * Secured all REST end points starting with '/api/v1/' path with OAuth2 tokens.
		 * To access all GET type methods, access token should have at least read scope
		 * and for all other methods(POST/PUT/DELETE), access token should have write
		 * scope
		 */
		/// api/v1/restaurent/new-restaurent
		/*
		 * http.cors().and().csrf().disable().authorizeRequests().antMatchers(
		 * "/api/v1/salstructure/upload-salaryStructure")
		 * .permitAll().antMatchers("/api/v1/organization/signupOrganization").permitAll
		 * () .antMatchers("/api/v1/organization/organizationRegistration").permitAll().
		 * antMatchers("/h2-console/login.jsp") .permitAll();
		 */
		
		http.cors().and().csrf().disable().authorizeRequests().antMatchers("/api/v1/organization/findOrganization",
				"/api/v1/organization/signupOrganization",
				"/api/v1/salstructure/upload-salaryStructure").permitAll()
				.antMatchers(HttpMethod.GET, SECURED_PATTERN).access(SECURED_READ_SCOPE)
				.antMatchers(SECURED_PATTERN).access(SECURED_WRITE_SCOPE);
	}



}

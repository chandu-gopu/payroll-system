package com.prutech.hr.payroll_system.oauth.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@WebFilter("/*")
public class OAuthCorsFilter implements Filter {
	//
	@Value("${access.control.allow.orgin}")
	private String accessControlAllowOrgin;
	//
	@Value("${access.control.allow.methods}")
	private String accessControlAllowMethods;
	//
	@Value("${access.control.allow.headers}")
	private String accessControlAllowHeaders;

	//
	public OAuthCorsFilter() {
		// empty OAuthCorsFilter()
	}

	/**
	 * 
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		final HttpServletResponse response = (HttpServletResponse) res;

		if (accessControlAllowOrgin == null) {
			accessControlAllowOrgin = "*";
		}
		if (accessControlAllowMethods == null) {
			accessControlAllowMethods = "*";
		}
		if (accessControlAllowHeaders == null) {
			accessControlAllowHeaders = "*";
		}
		response.setHeader("Access-Control-Allow-Origin", accessControlAllowOrgin);
		response.setHeader("Access-Control-Allow-Methods", accessControlAllowMethods);
		response.setHeader("Access-Control-Allow-Headers", accessControlAllowHeaders);

		if ("OPTIONS".equalsIgnoreCase(((HttpServletRequest) req).getMethod())) {
			response.setStatus(HttpServletResponse.SC_OK);
		} else {
			chain.doFilter(req, res);
		}
	}

	/**
	 * 
	 */
	@Override
	public void destroy() {
		// to destroy
	}

	/**
	 * 
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {
		// init method
	}
}
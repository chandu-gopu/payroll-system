package com.prutech.hr.payroll_system.oauth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PayRollEncoders {

	/**
	 * oauthClientPasswordEncoder 4 rounds
	 * 
	 * @return PasswordEncoder
	 */
	@Bean
	public PasswordEncoder oauthClientPasswordEncoder() {
		return new BCryptPasswordEncoder(4);
	}

	/**
	 * userPasswordEncoder 8 rounds
	 * 
	 * @return PasswordEncoder
	 */
	@Bean
	public PasswordEncoder userPasswordEncoder() {
		return new BCryptPasswordEncoder(8);
	}
}

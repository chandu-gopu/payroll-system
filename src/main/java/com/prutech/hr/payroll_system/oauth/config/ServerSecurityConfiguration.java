package com.prutech.hr.payroll_system.oauth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;

@Configuration
@CrossOrigin(origins = "*")
@EnableWebSecurity
//@EnableOAuth2Sso
//@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@Import(PayRollEncoders.class)
public class ServerSecurityConfiguration extends WebSecurityConfigurerAdapter {
	//
	@Autowired
	private UserDetailsService userDetailsService;
	//
	@Autowired
	private PasswordEncoder userPasswordEncoder;

	/**
	 * 
	 */
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	/**
	 * 
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(userPasswordEncoder);
	}

	/**
	 * Added
	 */
	@Bean
	public UserDetailsService userDetailsService() {
		return super.userDetailsService();
	}
}

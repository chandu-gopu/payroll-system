/*
 * package com.prutech.hr.payroll_system.service.impl;
 * 
 * import java.util.ArrayList; import java.util.List; import java.util.Optional;
 * 
 * import org.apache.log4j.Logger; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.core.io.support.SpringFactoriesLoader; import
 * org.springframework.stereotype.Service;
 * 
 * import com.prutech.hr.payroll.configuration.PayrollContext; import
 * com.prutech.hr.payroll.exception.PayRollServicesException; import
 * com.prutech.hr.payroll.model.PayRollLeaves; import
 * com.prutech.hr.payroll.model.SalaryComponent; import
 * com.prutech.hr.payroll.procedure.IProcedure; import
 * com.prutech.hr.payroll.repository.PayRollLeavesRepository; import
 * com.prutech.hr.payroll.service.SalaryStructureService; import
 * com.prutech.hr.payroll.util.PayrollCommonUtil; import
 * com.prutech.hr.payroll_system.h2db.model.Salary; import
 * com.prutech.hr.payroll_system.h2db.model.PaySlipComponent;
 * 
 * @Service public class PayRollServiceImpl {
 * 
 * private static Logger logger = Logger.getLogger(PayRollServiceImpl.class);
 * 
 * @Autowired private PayRollLeavesRepository payRollLeavesRepository;
 * 
 * @Autowired private SalaryStructureService salaryStructureService;
 * 
 * private String oganizationId;
 * 
 * public List<SalaryComponent> generatePayroll() { oganizationId =
 * PayrollCommonUtil.getLoggedInOrganizationId(); PayrollContext payrollContext
 * = new PayrollContext(PayrollCommonUtil.getLoggedInOrganizationId());
 * 
 * List<PayRollLeaves> listOfPayrollLeaves = payRollLeavesRepository
 * .findByMonthAndOrganizationId(payrollContext.getMonth(),
 * PayrollCommonUtil.getLoggedInOrganizationId());
 * 
 * if (!listOfPayrollLeaves.isEmpty()) { List<Salary> listOfPaySlips = new
 * ArrayList<Salary>(); List<SalaryComponent> listSalaryComponents = new
 * ArrayList<>(); listOfPayrollLeaves.forEach(payRollLeave -> {
 * 
 * Salary paySlip = new Salary(); logger.debug("Loading Services");
 * List<IProcedure> salProviders =
 * SpringFactoriesLoader.loadFactories(IProcedure.class, null);
 * 
 * List<PaySlipComponent> listOfPaySlipComponnets = new
 * ArrayList<PaySlipComponent>();
 * 
 * logger.debug("Excuting service providers" + salProviders.toString()); for
 * (IProcedure iprovider : salProviders) { salProviders.size();
 * Optional<SalaryComponent> salaryComponent;
 * iprovider.setPayrollContext(payrollContext); if
 * (salaryStructureService.checkComponentForEmployee(payRollLeave.
 * getEmployeeNumber(), oganizationId, iprovider.getComponentType())) {
 * 
 * salaryComponent =
 * iprovider.executeProcedure(payRollLeave.getEmployeeNumber(),
 * PayrollCommonUtil.getLoggedInOrganizationId()); logger.
 * debug("PayRollServiceImpl.generatePayroll().after executing stored procedure"
 * + salaryComponent.get().toString()); if (salaryComponent.isPresent()) {
 * listSalaryComponents.add(salaryComponent.get()); PaySlipComponent
 * paySlipComponet = new PaySlipComponent();
 * paySlipComponet.setCompensation(salaryComponent.get().getCompensation());
 * listOfPaySlipComponnets.add(paySlipComponet); //
 * //paySlip.setGrossPay(paySlip.getGrossPay().add(salaryComponent.get().
 * getPayableAmount())); } }
 * 
 * } listOfPaySlips.add(paySlip); }); return listSalaryComponents;
 * 
 * } else { throw new PayRollServicesException(
 * "Leaves Of employees Not provided for the month" +
 * payrollContext.getMonth()); }
 * 
 * }
 * 
 * }
 */
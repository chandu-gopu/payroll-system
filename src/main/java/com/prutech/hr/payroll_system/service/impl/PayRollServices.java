
package com.prutech.hr.payroll_system.service.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.SpringFactoriesLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.prutech.hr.constants.GlobalConstants;
import com.prutech.hr.enums.AttachmentEnums;
import com.prutech.hr.enums.EmployeeStatus;
import com.prutech.hr.model.Employee;
import com.prutech.hr.payroll.configuration.PayrollContext;
import com.prutech.hr.payroll.constants.PayrollConstants;
import com.prutech.hr.payroll.enums.ComponentType;
import com.prutech.hr.payroll.exception.PayRollServicesException;
import com.prutech.hr.payroll.h2db.model.EmployeeSalary;
import com.prutech.hr.payroll.h2db.model.TempSalaryComponent;
import com.prutech.hr.payroll.h2db.repository.EmployeeSalaryRepository;
import com.prutech.hr.payroll.h2db.repository.TempSalaryCompRepo;
import com.prutech.hr.payroll.model.PayRollLeaves;
import com.prutech.hr.payroll.model.PaySlip;
import com.prutech.hr.payroll.model.PaySlipComponent;
import com.prutech.hr.payroll.model.SalaryComponent;
import com.prutech.hr.payroll.procedure.IProcedure;
import com.prutech.hr.payroll.repository.PayRollLeavesRepository;
import com.prutech.hr.payroll.repository.PaySlipComponentRepository;
import com.prutech.hr.payroll.repository.PaySlipRepository;
import com.prutech.hr.payroll.service.SalaryStructureService;
import com.prutech.hr.payroll.util.PayrollCommonUtil;
import com.prutech.hr.repository.EmployeeRepository;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;

@Slf4j
@Service
public class PayRollServices {

	@Autowired
	private PayRollLeavesRepository payRollLeavesRepository;

	@Autowired
	private SalaryStructureService salaryStructureService;

	@Autowired
	private EmployeeSalaryRepository employeeSalaryRepository;

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private TempSalaryCompRepo tempSalaryCompRepo;
	
	@Autowired
	private PaySlipRepository paySlipRepository;
	
	@Autowired
	private PaySlipComponentRepository paySlipCompRepo;
	
	@Autowired
	private PayrollCommonUtil payrollCommonUtil;
	
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	
	private String organizationId;

	public List<EmployeeSalary> generatePayroll() throws InterruptedException, ExecutionException {
		log.debug("Generation Payroll Context");
		organizationId = PayrollCommonUtil.getLoggedInOrganizationId();
		PayrollContext payrollContext = new PayrollContext(PayrollCommonUtil.getLoggedInOrganizationId());
		log.debug("Gathering leavevs Informaton Of all the Employees"+payrollContext.getMonth());
		List<PayRollLeaves> listOfPayrollLeaves = getPayrollLeves(payrollContext.getMonth(),
				PayrollCommonUtil.getLoggedInOrganizationId());
		log.debug("Started Execution Of Payroll For each Employee Asynchonusly");
		Executor executor = Executors.newFixedThreadPool(100);
		CompletableFuture<List<EmployeeSalary>> payrollMainThread = CompletableFuture
				.supplyAsync(() -> executePayrollForEachEmployee(listOfPayrollLeaves, payrollContext), executor);
		List<EmployeeSalary> employeSalaries = payrollMainThread.get();

		return employeSalaries;
	}

	/**
	 * 
	 * @param listOfPayrollLeaves
	 * @param payrollContext
	 * @return
	 */
	private List<EmployeeSalary> executePayrollForEachEmployee(List<PayRollLeaves> listOfPayrollLeaves,
			PayrollContext payrollContext) {
		List<EmployeeSalary> employeSalarys = new ArrayList<>();
		List<CompletableFuture<EmployeeSalary>> employeeFurures = new ArrayList<>();
		for (PayRollLeaves employeeLeaves : listOfPayrollLeaves) {
			Executor executor = Executors.newFixedThreadPool(200);

			if (checkEmployeeStatus(employeeLeaves.getEmployeeNumber())) {
				CompletableFuture<EmployeeSalary> employeeThread = CompletableFuture
						.supplyAsync(() -> executeForSingleEmployee(employeeLeaves, payrollContext), executor)
						.thenApply(salary -> {
							employeSalarys.add(salary);
							return salary;
						});
				employeeFurures.add(employeeThread);

			}

		}

		for (CompletableFuture<EmployeeSalary> employeeFuture : employeeFurures) {
			try {
				employeeFuture.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		return employeSalarys;

	}

	/**
	 * 
	 * @param employeeNumber
	 * @return
	 */
	private boolean checkEmployeeStatus(String employeeNumber) {
		Optional<Employee> employee = employeeRepository.findByEmployeeNumberAndStatusAndOrganization_OrganizationId(
				employeeNumber, EmployeeStatus.ACTIVE, organizationId);

		if (employee.isPresent()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param employeeLeaves
	 * @param payrollContext
	 * @return
	 * @return
	 */

	private EmployeeSalary executeForSingleEmployee(PayRollLeaves employeeLeaves, PayrollContext payrollContext) {

		EmployeeSalary employeeSalary = new EmployeeSalary();
		employeeSalary.setEmployeeNumber(employeeLeaves.getEmployeeNumber());
		employeeSalary.setMonth(employeeLeaves.getMonth());
		employeeSalary.setEmployeeName(employeeRepository
				.findByEmployeeNumberAndOrganization_OrganizationId(employeeLeaves.getEmployeeNumber(), organizationId)
				.get().getFirstName());
		employeeSalary.setNoOfWorkingDays(payrollContext.getWorkingDays());
		employeeSalary.setLossOfPayDays(employeeLeaves.getLossOfPayInDays());
		employeeSalary.setOrganizationId(organizationId);
		List<IProcedure> serviceProviders = SpringFactoriesLoader.loadFactories(IProcedure.class, null);
		log.info("Execution of Services for employee :" + employeeLeaves.getEmployeeNumber() + " : Started");
	    BigDecimal totalEarnings=executeComponents(serviceProviders,employeeLeaves,payrollContext,employeeSalary,ComponentType.EARNING);
		BigDecimal contributions=executeComponents(serviceProviders,employeeLeaves,payrollContext,employeeSalary,ComponentType.CONTRIBUTION);
		employeeSalary.setGrossPay(totalEarnings.add(contributions));
		employeeSalaryRepository.save(employeeSalary);
		BigDecimal deductions=executeComponents(serviceProviders,employeeLeaves,payrollContext,employeeSalary,ComponentType.DEDUDUCTION);

		employeeSalary.setNetPayable(totalEarnings.subtract(contributions).subtract(deductions));
		employeeSalaryRepository.save(employeeSalary);
		log.info("Execution of Services for employee :" + employeeLeaves.getEmployeeNumber() + " : Completed");
		return employeeSalary;
	}

	
	/**
	 * @param serviceProviders
	 * @param employeeLeaves
	 * @param payrollContext
	 * @param employeeSalary
	 * @param deduduction
	 * @return
	 */
	private BigDecimal executeComponents(List<IProcedure> serviceProviders, PayRollLeaves employeeLeaves,
			PayrollContext payrollContext, EmployeeSalary employeeSalary, ComponentType componentsType) {
		String method =new Throwable().getStackTrace()[0].getMethodName();
		log.debug(GlobalConstants.ENTERED_INTO_METHOD.replace("@@method@@", method));
		log.debug(method+"Execution of :"+componentsType+"Started for employee :"+employeeLeaves.getEmployeeNumber());
		List<CompletableFuture<List<TempSalaryComponent>>> futuresOfAllSPIS = new ArrayList<>();
		List<TempSalaryComponent> components = new ArrayList<TempSalaryComponent>();
		Executor executor = Executors.newFixedThreadPool(100);
		for (IProcedure iService : serviceProviders) {
			if(iService.getComponentType()==componentsType) {
			if (salaryStructureService.checkComponentForEmployee(employeeLeaves.getEmployeeNumber(), organizationId,
					iService.getComponentName())) {
				iService.setPayrollContext(payrollContext);
				futuresOfAllSPIS.add(CompletableFuture.supplyAsync(() -> executeService(iService, employeeLeaves), executor)
						.thenApply((com) -> {
							for (TempSalaryComponent tempSalaryComponent : com) {
								tempSalaryCompRepo.save(tempSalaryComponent);
								components.add(tempSalaryComponent);
							}
							
							return components;
						}));
			}
			}
		}
		futuresOfAllSPIS.forEach(future -> {
			try {
				future.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		});
		
		
		switch (componentsType) {
		case EARNING:
			employeeSalary.setEarnings(components);
			employeeSalary.setGrossPay(calculateTotal(components));
	
			break;
		case DEDUDUCTION:
			employeeSalary.setDeductions(components);
			break;
		case CONTRIBUTION:
			employeeSalary.setContributions(components);
			break;
		default:
			break;
		}
		
			
		employeeSalaryRepository.save(employeeSalary);
		log.debug("Execution of :"+componentsType+"Completed for employee :"+employeeLeaves.getEmployeeNumber());
		log.debug(GlobalConstants.EXIT_FROM_METHOD.replace("@@method@@", method));
		employeeSalaryRepository.save(employeeSalary);
		return calculateTotal(components);
	
	}


	/**
	 * @param eranings
	 * @return
	 */
	private BigDecimal calculateTotal(List<TempSalaryComponent> eranings) {
		BigDecimal totalAmount=new BigDecimal(0.0);
		for (TempSalaryComponent tempSalaryComponent : eranings) {
			
			totalAmount=totalAmount.add(tempSalaryComponent.getCompensation());
		}
		return totalAmount;
	}

	/**
	 * 
	 * @param iService
	 * @param employeeLeaves
	 * @return
	 */
	private List<TempSalaryComponent> executeService(IProcedure iService, PayRollLeaves employeeLeaves) {
		
		String method =new Throwable().getStackTrace()[0].getMethodName();
	List<TempSalaryComponent> tempSalaryComponents=new ArrayList<>(1);
	TempSalaryComponent tempSalaryComponent=new TempSalaryComponent();
	tempSalaryComponent.setComponentName(iService.getComponentName());
		log.info(method+"executing " + iService.getComponentName() + " for :" + employeeLeaves.getEmployeeNumber()
				+ "  Started");
		Optional<SalaryComponent> salaryComponent = iService.executeProcedure(employeeLeaves.getEmployeeNumber(),
				organizationId);
		if (salaryComponent.isPresent()) {
			switch (iService.getComponentName()) {
			case PayrollConstants.EPF:
				tempSalaryComponent.setCompensation(salaryComponent.get().getEmployeePF());
				tempSalaryComponent.setComponentType(ComponentType.CONTRIBUTION);
				TempSalaryComponent employerPF=new TempSalaryComponent();
				employerPF.setCompensation(salaryComponent.get().getEmployerPF());
				employerPF.setComponentName(PayrollConstants.EMPLOYER_PF);
				employerPF.setComponentType(ComponentType.CONTRIBUTION);
				tempSalaryComponents.add(tempSalaryCompRepo.save(employerPF));
				break;
           case PayrollConstants.ESI:
        	   tempSalaryComponent.setCompensation(salaryComponent.get().getEmployesContribution());
				tempSalaryComponent.setComponentType(ComponentType.CONTRIBUTION);
				break;    
			default:
				tempSalaryComponent.setCompensation(salaryComponent.get().getPayableAmount());
				tempSalaryComponent.setComponentType(ComponentType.EARNING);
				break;
			}
		}
		tempSalaryComponents.add(tempSalaryCompRepo.save(tempSalaryComponent));
		log.info(method+"executing " + iService.getComponentName() + " for :" + employeeLeaves.getEmployeeNumber()
		+ " Completed");
		return tempSalaryComponents;
		
	}

	/**
	 * Get Payroll leaves for the Given Month
	 * 
	 * @param month
	 * @param loggedInOrganizationId
	 * @return
	 */
	private List<PayRollLeaves> getPayrollLeves(String month, String loggedInOrganizationId) {
		List<PayRollLeaves> payRollLeaves = payRollLeavesRepository.findByMonthAndOrganizationId(month,
				loggedInOrganizationId);

		if (!payRollLeaves.isEmpty()) {
			return payRollLeaves;

		} else {
			throw new PayRollServicesException("Payroll Leaves Are Not Defined For the Month :" + month);
		}

	}

	/**
	 * 
	 * @param month
	 * @return
	 * @throws InterruptedException
	 */
	public List<EmployeeSalary> getEmployeeTempSalaries(String month) {

		List<EmployeeSalary> emplSalaries = employeeSalaryRepository.findByMonthAndOrganizationId(month,
				PayrollCommonUtil.getLoggedInOrganizationId());

		return emplSalaries;

	}

	/**
		 * 
		 * @param 
		 * @return
		 */
	public List<PaySlip> ApproveSalariesAndSaveInDB(List<String> employeNumbersList) {
	       List<PaySlip> paySlips=new ArrayList<>(1);
		for (String employeeNunber : employeNumbersList) {
			EmployeeSalary employeeSalary=employeeSalaryRepository.findByEmployeeNumberAndOrganizationId(employeeNunber, PayrollCommonUtil.getLoggedInOrganizationId());
			PaySlip paySlip=new PaySlip();
			paySlip=dozerBeanMapper.map(employeeSalary, PaySlip.class);
			paySlip.setEarnings(getComponents(employeeSalary,ComponentType.EARNING));
			paySlip.setContributions(getComponents(employeeSalary,ComponentType.CONTRIBUTION));
			
			paySlip.setNetPayable(PayrollCommonUtil.calculateTotal(paySlip.getEarnings()));
			paySlip.setSalaryInwords(PayrollCommonUtil.convertToIndianCurrency(paySlip.getNetPayable().toPlainString()));
			paySlipRepository.save(paySlip);
			paySlips.add(paySlip);
			
		}
		return paySlips;
		
	}
	

	

	/**
	 * @param employeeSalary
	 * @return
	 */
	private List<PaySlipComponent> getComponents(EmployeeSalary employeeSalary,ComponentType componentType) {
		
		String method =new Throwable().getStackTrace()[0].getMethodName();
		
		List<PaySlipComponent> components=new ArrayList<>();
		switch (componentType) {
		case EARNING:
			for (TempSalaryComponent paySlipComponent : employeeSalary.getEarnings()) {
				components.add(dozerBeanMapper.map(paySlipComponent, PaySlipComponent.class));
			}
			return components;
			
		case CONTRIBUTION:
			for (TempSalaryComponent paySlipComponent : employeeSalary.getContributions()) {
				components.add(dozerBeanMapper.map(paySlipComponent, PaySlipComponent.class));
			}
			return components;
		case  DEDUDUCTION:
			for (TempSalaryComponent paySlipComponent : employeeSalary.getDeductions()) {
				components.add(dozerBeanMapper.map(paySlipComponent, PaySlipComponent.class));
			}
			return components;
		default:
		log.debug(method+PayrollConstants.COMPONENTTYPE_NOT_EXISTS+employeeSalary.getEmployeeNumber());
		 throw new PayRollServicesException(method+PayrollConstants.COMPONENTTYPE_NOT_EXISTS+employeeSalary.getEmployeeNumber());
			
		}
		
	}


	
	/**
	 * 
	 * @throws IOException
	 * @throws SQLException
	 * @throws JRException
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW,readOnly = false)
	public void generatePaySlips() throws IOException, SQLException, JRException {
		log.info("Started the  creating the pay slips using JasperReports");
		List<PaySlip> PaySlips = paySlipRepository.findAll();

		JRPdfExporter exporter = new JRPdfExporter();
		SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
		reportConfig.setSizePageToContent(true);
		reportConfig.setForceLineBreakPolicy(false);

		SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
		exportConfig.setMetadataAuthor("Chandu");
		exportConfig.setEncrypted(true);
		exportConfig.setAllowedPermissionsHint("PRINTING");

		exporter.setConfiguration(reportConfig);
		exporter.setConfiguration(exportConfig);
		Resource reportresource = new ClassPathResource("jasperReport.jrxml");
		Resource imageResource = new ClassPathResource("prutech.png");
		
		for (PaySlip employeeSalary : PaySlips) {
			List<PaySlip> employeeSalaries = new ArrayList<>();
	     	String month  =	employeeSalary.getMonth().split("-")[0];	
	     	employeeSalary.setMonth(month);
		    employeeSalaries.add(employeeSalary);
			InputStream reportStream = reportresource.getInputStream();
			InputStream logoImage = imageResource.getInputStream();
			JRBeanCollectionDataSource earnings = new JRBeanCollectionDataSource(employeeSalary.getEarnings());
			JRBeanCollectionDataSource contributions=new JRBeanCollectionDataSource(employeeSalary.getContributions());
			JRBeanCollectionDataSource employeeBean = new JRBeanCollectionDataSource(employeeSalaries);
			JRBeanCollectionDataSource salBean = new JRBeanCollectionDataSource(employeeSalaries);
			JRBeanCollectionDataSource deductions=new JRBeanCollectionDataSource(employeeSalary.getDeductions());
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("Earnings", earnings);
			parameters.put("Contributions", contributions);
			parameters.put("EmployeeInfo", employeeBean);
			parameters.put("logo", logoImage);
			parameters.put("SalaryDetails", salBean);
			parameters.put("Deductions", deductions);
			JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			String payslipPath=uploadDocument(JasperExportManager.exportReportToPdf(jasperPrint),employeeSalary.getEmployeeName());
			employeeSalary.setFilePath(payslipPath);
			paySlipRepository.save(employeeSalary);
			exporter.setExporterOutput(
					new SimpleOutputStreamExporterOutput(employeeSalary.getEmployeeName() + "--PaySlip.pdf"));
			exporter.exportReport();
			log.info("Completed the  creating the pay slips using JasperReports");

		}
	}

	
	
	/**
	 * @param exportReportToPdf
	 * @return
	 */
	private String uploadDocument(byte[] exportReportToPdf,String employeeName) {
		com.prutech.hr.model.Document uploadedFilePath = null;
		Encoder encoder = Base64.getEncoder();
		String encodedFile = encoder.encodeToString(exportReportToPdf);
		Map<String, String> map = payrollCommonUtil.createPath(employeeName+"--Payslip.pdf", AttachmentEnums.PDF);
		uploadedFilePath = payrollCommonUtil.createJsonObjectForFileProcess(employeeName+"--Payslip.pdf", encodedFile, AttachmentEnums.PDF, map);
		return uploadedFilePath.getUploadingPath();
		
	}
	

	@Transactional(propagation = Propagation.REQUIRES_NEW,readOnly = false)
	public void generateEncryptedPayslips() throws IOException, JRException, DocumentException {
		List<PaySlip> PaySlips = paySlipRepository.findByOrganizationId(PayrollCommonUtil.getLoggedInOrganizationId());
		
		Resource reportresource = new ClassPathResource("jasperReport.jrxml");
		Resource imageResource = new ClassPathResource("prutech.png");
		
		for (PaySlip employeeSalary : PaySlips) {
			List<PaySlip> employeeSalaries = new ArrayList<>();
	     	String month  =	employeeSalary.getMonth().split("-")[0];	
	     	employeeSalary.setMonth(month);
		    employeeSalaries.add(employeeSalary);
			InputStream reportStream = reportresource.getInputStream();
			InputStream logoImage = imageResource.getInputStream();
			JRBeanCollectionDataSource earnings = new JRBeanCollectionDataSource(employeeSalary.getEarnings());
			JRBeanCollectionDataSource contributions=new JRBeanCollectionDataSource(employeeSalary.getContributions());
			JRBeanCollectionDataSource employeeBean = new JRBeanCollectionDataSource(employeeSalaries);
			JRBeanCollectionDataSource salBean = new JRBeanCollectionDataSource(employeeSalaries);
			JRBeanCollectionDataSource deductions=new JRBeanCollectionDataSource(employeeSalary.getDeductions());
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("Earnings", earnings);
			parameters.put("Contributions", contributions);
			parameters.put("EmployeeInfo", employeeBean);
			parameters.put("logo", logoImage);
			parameters.put("SalaryDetails", salBean);
			parameters.put("Deductions", deductions);
			JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			Document document = new Document(PageSize.A4);
			String destination=employeeSalary.getEmployeeName()+"--payslip.pdf";
			
			FileOutputStream fos= new FileOutputStream(destination);
			
			fos.write(JasperExportManager.exportReportToPdf(jasperPrint));
			
			PdfWriter writer = PdfWriter.getInstance(document, fos);
			
			String user=employeeSalary.getEmployeeName();
		
			String owner=employeeSalary.getOrganizationId();
			writer.setEncryption( user.getBytes(),
	                owner.getBytes(), PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING,
	                PdfWriter.ENCRYPTION_AES_256 | PdfWriter.DO_NOT_ENCRYPT_METADATA);
			
			    document.open();
		        document.add(new Paragraph("Secure Pdf with Password using iText"));
		        document.close();
		//	String payslipPath=uploadDocument(JasperExportManager.exportReportToPdf(jasperPrint),employeeSalary.getEmployeeName());
			//employeeSalary.setFilePath(payslipPath);
			paySlipRepository.save(employeeSalary);
		
			log.info("Completed the  creating the pay slips using JasperReports");

		}
		
		
		
		
		
		
	}
	 
}

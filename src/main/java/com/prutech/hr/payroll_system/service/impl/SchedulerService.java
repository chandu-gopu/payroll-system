package com.prutech.hr.payroll_system.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import com.prutech.hr.payroll.util.PayrollCommonUtil;
import com.prutech.hr.repository.EmployeeRepository;
import com.prutech.scheduler.JobSchedulerService;
import com.prutech.scheduler.model.CronJob;

@Service
public class SchedulerService {

	@Autowired
	JobSchedulerService mySchedulerService;

	@Autowired
	EmployeeRepository employeeRepository;

	/**
	 * 
	 */
	public CronJob scheduleJob(CronJob job, Class<? extends QuartzJobBean> clazz) throws Exception {
		job.setGroupName(PayrollCommonUtil.getLoggedInOrganizationId());

		if (job.getTriggerType() == 0) {
			mySchedulerService.scheduleCronJob(clazz, job.getJobName(), job.getGroupName(), new Date(),
					job.getCronExpression(), 0, null);
		} else {
			mySchedulerService.sheduleSimpleTrigger(clazz, job.getJobName(), job.getGroupName(), new Date(),
					job.getRepeatCount(), job.getRepeatInterval(), 0, null);
		}
		return job;
	}

}
